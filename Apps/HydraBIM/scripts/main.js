var panelVisible = false

function togglePanel() {
    $('#cesiumContainer').toggleClass('col-12');
    $('#cesiumContainer').toggleClass('col-9');
    $('#sidebar').toggleClass('d-none');
    panelVisible = !panelVisible
}

function showSidebar() {
    if (!panelVisible) {
        togglePanel();
    }
}

function showPopup() {
    window.open('https://mapy.geoportal.gov.pl/imap/Imgp_2.html?gpmap=gp0', '_blank', 'toolbar=0,menubar=0,width=600,height=400');
}

$(document).ready(function () {
    Cesium.Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzYjViNDM5OC1iY2UyLTQxNGUtOTczNS0xNTlmNzlmYWM1MjUiLCJpZCI6ODgzODMsImlhdCI6MTY0OTE4Mjk2MH0.EHZCbl60MZiJATxbOW9JI3penKxNf03fsV-rrtQfqc0";
    
    // An example of using a b3dm tilset to classify another b3dm tileset.
    const viewer = new Cesium.Viewer("cesiumContainer", {
        terrainProvider: Cesium.createWorldTerrain(),
    });

    // A normal b3dm tileset of photogrammetry
    const tileset = new Cesium.Cesium3DTileset({
        url: Cesium.IonResource.fromAssetId(40866),
        instanceFeatureIdLabel: 99
    });
    viewer.scene.primitives.add(tileset);
    viewer.zoomTo(tileset);

    const classifcationTilesetUrl =
        "../SampleData/Cesium3DTiles/Classification/Photogrammetry/tileset.json";
    // A b3dm tileset used to classify the photogrammetry tileset
    const classificationTileset = new Cesium.Cesium3DTileset({
        url: classifcationTilesetUrl,
        classificationType: Cesium.ClassificationType.CESIUM_3D_TILE,
    });
    classificationTileset.style = new Cesium.Cesium3DTileStyle({
        color: "rgba(255, 0, 0, 0.5)",
    });
    classificationTileset.name = "Jakis budynek";
    viewer.scene.primitives.add(classificationTileset);

    // An entity object which will hold info about the currently selected feature for infobox display
    const selectedEntity = new Cesium.Entity();

    // Get default left click handler for when a feature is not picked on left click
    const clickHandler = viewer.screenSpaceEventHandler.getInputAction(
        Cesium.ScreenSpaceEventType.LEFT_CLICK
    );

    viewer.screenSpaceEventHandler.setInputAction(function onLeftClick(movement) {
        const pickedFeature = viewer.scene.pick(movement.position);
        if (!Cesium.defined(pickedFeature)) {
            clickHandler(movement);
            return;
        }

        // Set feature infobox description
        const featureName = pickedFeature.getProperty("name");
        console.log(pickedFeature);

        showSidebar();
        $('#objectName').html('You clicked a building!');

    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);


    $('#testBtn').click(function () {
        togglePanel();
    });

    $('#showPopup').click(function () {
        showPopup();
    });

    console.log(classificationTileset);

});

